public class EnergyDrink{
	private String Brand;
	private int Volume;
	private int mgOfCaffeine;
	
	public String getBrand(){
		return this.Brand;
	}
	public int getVolume(){
		return this.Volume;
	}
	public int getmgOfCaffeine(){
		return this.mgOfCaffeine;
	}
	
	public void setBrand(String Brand){
		this.Brand = Brand;
	}
	public void setVolume(int Volume){
		this.Volume = Volume;
	}
	public void setmgOfCaffeine(int mgOfCaffeine){
		this.mgOfCaffeine = mgOfCaffeine;
	}
	EnergyDrink(String Brand, int Volume, int mgOfCaffeine){
		this.Brand = Brand;
		this.Volume = Volume;
		this.mgOfCaffeine = mgOfCaffeine;
	}
	
	public int addCaffeine(int mgOfCaffeine){
		int totalCaffeine = mgOfCaffeine + 100;
		return totalCaffeine;
	}
}